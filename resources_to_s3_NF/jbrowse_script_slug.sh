#!/bin/bash
#
# I am going to assume the CSV files have been patched with the s3 basename path - field 7 for tracks
#

# Genomes
tail -n +1 genomes.csv | # it should not be genomes
sort -t , -k1,1 |
awk -F, '{ print "jbrowse add-assembly https://s3.embl.de/annelids/"$1"/"$2" \
-n \""$1"\" --displayName \""$3"\"" }'

tail -n +1 tracks.csv | # it should not be tracks
sort -t , -k1,1 -k5,5 -k2,2 |
awk -F, '{ print "jbrowse add-track https://s3.embl.de/annelids/"$1"/"$5"/"$3".vcf.gz \
-a \""$1"\" -d \""$6"\" -n \""$3"\" --category \""$5"\""}'

# Text-indices
tail -n +1 tracks.csv |
awk '$1 {print;}'
