# Preparing tracks for Jbrowse2

Jbrowse2 typically needs compressed and index files using `bgzip` and `tabix` from `HTSlib` or `SAMtools`.
This folder contains a Nextflow pipeline that will:

* read CSV files `genomes.csv` and `tracks.csv` to collect the requirements
* make sure the files are accessible
* do whatever processing is needed
* upload it to an EMBL S3 bucket
* provide a template to populate the `../image/jbrowse_script.sh` file used for Jbrowse2 Docker image

In theory you would have to edit those CSV files, rerun the pipeline and commit the resulting files in order to update Jbrowse2 with new tracks.

## S3 configuration

- `nextflow secrets set ACCESS_KEY ""`
- `nextflow secrets set SECRET_KEY ""`

## Launching the workflow

To launch:
- `module load Nextflow`
- `nextflow run -c nextflow.config -w /scratch/cros/nextflow-workdir -params-file params.yaml jbrowse2_processing.nf -resume`

## Citations required

* [Sequenceserver: A modern graphical user interface for custom BLAST databases. Molecular Biology and Evolution (2019).⁠](https://doi.org/10.1093/molbev/msz185)
* [JBrowse 2: a modular genome browser with views of synteny and structural variation. Genome Biology (2023).](https://doi.org/10.1186/s13059-023-02914-z)
