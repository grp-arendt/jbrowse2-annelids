#!/usr/bin/env nextflow
nextflow.enable.dsl=2

process compress_genomes {
    tag "$genome"
    label "htslib"
    label "small_job"
    input:
        tuple val(genome), path("${genome}.fa")
    output:
        tuple val(genome), path("${genome}.fa.gz")
    """
    echo $genome
    bgzip --threads $task.cpus -c ${genome}.fa > ${genome}.fa.gz
    ls -alh
    """
}

process index_genomes {
    tag "$genome"
    label "samtools"
    label "small_job"
    publishDir "s3://$params.s3_bucket/$genome"
    input:
        tuple val(genome), path("${genome}.fa.gz")
    output:
        tuple val(genome), path("${genome}.fa.gz"), path("${genome}.fa.gz.gzi"), path("${genome}.fa.gz.fai")
    """
    echo $genome
    samtools faidx ${genome}.fa.gz --fai-idx ${genome}.fa.gz.fai --gzi-idx ${genome}.fa.gz.gzi
    ls -alh
    """
}

process gtf_to_gff_agat {
    tag "$id"
    label "agat"
    label "small_job"
    input:
        tuple val(id), val(dest), val(type), path("${id}.gtf")
    output:
        tuple val(id), val(dest), val(type), path("${id}.gff")
    """
    echo $id
    agat_convert_sp_gxf2gxf.pl -g ${id}.gtf -o ${id}.gff
    """
}

process vcf_bcftools {
    tag "$id"
    label "bcftools"
    label "small_job"
    publishDir "$dest"
    input:
        tuple val(id), val(dest), val(type), path("${id}.unsorted.vcf")
    output:
        tuple val(id), val(dest), val(type), path("${id}.vcf.gz"), path("${id}.vcf.gz.tbi")
    """
    echo $id
    bcftools sort ${id}.unsorted.vcf > ${id}.vcf
    bcftools view  ${id}.vcf --output-type z > ${id}.vcf.gz
    bcftools index --tbi ${id}.vcf.gz
    ls -alh
    """
}

process index_compress_sort_gff {
    tag "$id"
    label "htslib"
    label "small_job"
    publishDir "$dest"
    input:
        tuple val(id), val(dest), val(type), path(gff)
    output:
        tuple val(id), val(dest), val(type), path("${gff}.gz"), path("${gff}.gz.tbi")
    """
    echo $id
    (grep "^#" $gff; grep -v "^#" $gff | sort -t"`printf '\t'`" -k1,1 -k4,4n) > sorted_${gff};
    bgzip --threads $task.cpus -i -c sorted_${gff} > ${gff}.gz
    tabix -f ${gff}.gz > ${gff}.gz.tbi
    ls -alh
    """
}

process index_samtools {
    tag "$id"
    label "samtools"
    label "small_job"
    publishDir "$dest"
    input:
        tuple val(id), val(dest), val(type), path(bam)
    output:
        tuple val(id), val(dest), val(type), path("${bam}"), path("${bam}.bai")
    """
    echo $id
    samtools index --threads $task.cpus -b ${bam} > ${bam}.bai
    ls -alh
    """
}

process upload_only {
    tag "$id"
    label "small_job"
    publishDir "$dest"
    input:
        tuple val(id), val(dest), val(type), path(infile)
    output:
        tuple val(id), val(dest), val(type), path("${infile}")
    """
    echo $id
    ls -alh
    """
}

workflow {
    Channel.fromPath(params.csv_genomes_to_process, checkIfExists: true) |
        splitCsv(header: true) |
        set {genomes}
    Channel.fromPath(params.csv_tracks_to_process, checkIfExists: true) |
        splitCsv(header: true) |
        set {tracks}
    // Simple checking for the genome files path
    genomes |
        map { it.path } |
        concat( tracks.map( { it.path } ) ) |
        map { file(it, checkIfExists: true) }
    ///////////////////// GENOME PARTS
    // Compressing the genomes and uploading them
    genomes |
        map {it -> [it.name, it.path]} |
        compress_genomes |
        index_genomes |
        map{ it -> [ it[0], "https://$params.s3_url/$params.s3_bucket/" + it[0] + "/" + it[1].name, \
                    "https://$params.s3_url/$params.s3_bucket/" + it[0] + "/" + it[2].name,\
                    "https://$params.s3_url/$params.s3_bucket/" + it[0] + "/" + it[3].name ]} |
        combine (genomes.map(it -> [it.name, it.display, it.path_to_LUT]), by: 0) |
        map{ it -> [assembly: it[0], s3: it[1], s3_idx_gzi: it[2], s3_idx_fai: it[3], display: it[4], LUT: it[5]]} |
        map {it -> "jbrowse add-assembly ${it.s3} -n \"${it.assembly}\" --displayName \"${it.display}\" " + \
                    "--gziLocation ${it.s3_idx_gzi} --faiLocation ${it.s3_idx_fai} -t bgzipFasta " + \
                    ((it.LUT.isEmpty()) ? " " : " --refNameAliases ${it.LUT}" )} |
        collectFile(name: 'jbrowse_add_assemblies.sh', newLine: true, storeDir: params.out_dir, sort: true)
    ///////////////////// TRACK PART
    // Split by types
    tracks |
        map {it -> [id: it.assembly+"_"+it.category+"_"+it.trackID, \
                    dest: "s3://$params.s3_bucket" + "/" + it.assembly +"/"+it.category, \
                    type: it.type, path: it.path ]} |
        branch {
            gff: it.type == 'GFF'
            gtf: it.type == 'GTF'
            vcf: it.type == 'VCF'
            bam: it.type == 'BAM'
            just_upload_it: it.type == 'BigWig' || it.type == 'BigBed'
            not_handled: true
        } |
        set {track_by_type}
    // Do GTFs/GFFs
    track_by_type.gff |
        concat(gtf_to_gff_agat(track_by_type.gtf)) |
        index_compress_sort_gff |
        set {GFF}
    // VCF
    track_by_type.vcf |
        vcf_bcftools |
        set {VCF}
    // BAM
    track_by_type.bam |
        index_samtools |
        set {BAM}
    // BigWig and other files you just upload
    track_by_type.just_upload_it |
        upload_only |
        map { it -> it + [""]} |
        set {track_with_no_idx}
    track_by_type.not_handled |
        view{"Nothing gets done with tracks of unhandled type $it"}
    // Reunits those files for track downprocessing
    track_with_no_idx |
        concat(GFF,VCF, BAM) |
        // ID / s3 / s3 index
        map {it -> [it[0], file(it[3]).getName(), (it[4].isEmpty()) ? "" :  file(it[4]).getName()]} |
        set {merged_tracks}
    // Add back the early info
    tracks |
        map {it -> [it.assembly+"_"+it.category+"_"+it.trackID] + it.values()} |
        combine(merged_tracks, by:0) |
        map {it -> [assembly: it[1], trackId: it[2], name: it[3], \
                    category: it[5], desc: it[6], \
                    s3: "https://" + params.s3_url +"/"+params.s3_bucket+"/"+ it[1]+ "/" + it[5] + "/" + it[9], \
                    s3_idx: "https://" + params.s3_url +"/"+params.s3_bucket+"/"+ it[1]+ "/" + it[5] + "/" + it[10], \
                    text_idx: it[8]
        ]} |
        set {processed_tracks}
    processed_tracks |
        map {it -> "jbrowse add-track ${it.s3} -a \"${it.assembly}\" -d \"${it.desc}\" -n \"${it.name}\" " + \
                    "--category \"${it.category}\" --trackId \"${it.trackId}\" " + \
                    ((it.s3_idx.isEmpty()) ? " " : " --indexFile ${it.s3_idx}" )} |
        collectFile(name: 'jbrowse_add_tracks.sh', newLine: true, storeDir: params.out_dir, sort: true)
}

workflow.onComplete {
	log.info ( workflow.success ? "\nWorkflow exited successfully!" : "\nOops .. something went wrong" )
}
