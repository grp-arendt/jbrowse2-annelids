#!/bin/bash
module load HTSlib BCFtools
export WORK_DIR=`mktemp -d`
echo $WORK_DIR
mkdir $WORK_DIR/variation
files=($(cut -d',' -f 7 tracks.csv | grep "gerber" | tr -d '"'))

## now loop through the above array
for vcf in "${files[@]}"
do
    vcf_mod=$(basename $vcf |  sed 's/^.*_\([^_]*\)$/\1/')
    echo "Processing $vcf_mod"
    bcftools sort $vcf > $WORK_DIR/$vcf_mod
    bgzip -c $WORK_DIR/$vcf_mod > $WORK_DIR/variation/${vcf_mod}.gz
    rm $WORK_DIR/$vcf_mod
    tabix $WORK_DIR/variation/${vcf_mod}.gz
done
