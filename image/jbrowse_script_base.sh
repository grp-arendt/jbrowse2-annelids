#!/bin/bash -eux
# Platy only for now
jbrowse create annelids
cd annelids
# Assemblies
jbrowse add-assembly https://s3.embl.de/annelids/pdumv021/pdumv021.fa.gz -n "pdumv021" --displayName "Platynereis dumerilii genome v2.1 (pdumv021)" --gziLocation https://s3.embl.de/annelids/pdumv021/pdumv021.fa.gz.gzi --faiLocation https://s3.embl.de/annelids/pdumv021/pdumv021.fa.gz.fai -t bgzipFasta
jbrowse add-assembly https://s3.embl.de/annelids/pmassv1_masked/pmassv1_masked.fa.gz -n "pmassv1_masked" --displayName "Platynereis massiliensis genome v1 - masked (pmassv1)" --gziLocation https://s3.embl.de/annelids/pmassv1_masked/pmassv1_masked.fa.gz.gzi --faiLocation https://s3.embl.de/annelids/pmassv1_masked/pmassv1_masked.fa.gz.fai -t bgzipFasta
# Reference annotation
jbrowse add-track https://s3.embl.de/annelids/pdumv021/Annotation/pdumv021_Annotation_annotation.gff.gz -a "pdumv021" -d "Kevin's GTF file" -n "Gene models" --category "Annotation" --trackId "annotation"  --indexFile https://s3.embl.de/annelids/pdumv021/Annotation/pdumv021_Annotation_annotation.gff.gz.tbi
jbrowse add-track https://s3.embl.de/annelids/pmassv1_masked/Annotation/pmassv1_masked_Annotation_annotation.gff.gz -a "pmassv1_masked" -d "Kevin's GTF file" -n "Gene models" --category "Annotation" --trackId "annotation-pmass"  --indexFile https://s3.embl.de/annelids/pmassv1_masked/Annotation/pmassv1_masked_Annotation_annotation.gff.gz.tbi
# Text index
jbrowse text-index
