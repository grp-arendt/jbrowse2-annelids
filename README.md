# Jbrowse2 Annelids

## Description

This project intends to manage a Jbrowse2 deployment for annelid genomes in general and *Platynereis dumerilii* in particular.

Jbrowse2 is a genome browser with attractive features, notably that we can add tracks of interest corresponding to variations (VCFs), long read sequencing (GFFs from Pacbio), HI-C, ATACseq runs (BAM).

 The publication reference is [*JBrowse 2: a modular genome browser with views of synteny and structural variation. Genome Biology (2023).*](https://doi.org/10.1186/s13059-023-02914-z)

## Access

Currently I use a branch based system to deploy resources using Gitlab. Here is what is available:
- https://genomes.arendt.embl.de : publicly available resources, from branch `main`
- https://genomes-dev.arendt.embl.de : some unpublished resources for internal lab use, from branch `dev`

## Adding tracks

Go to the `resources_to_s3_NF` folder. There is Nextflow job (work in progress) that will process and upload to S3 the genomic tracks, and produce a new script to add the tracks to Jbrowse2. This scripts then goes to `image/jbrowse_script.sh`.

* `genomes.csv` spreadsheet for the assemblies
* `tracks.csv` for the genomic tracks to add.
